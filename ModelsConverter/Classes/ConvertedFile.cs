﻿using Symbiant.ModelsConverter.Enums;
using Symbiant.ModelsConverter.Extensions;
using Symbiant.ModelsConverter.Models;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace Symbiant.ModelsConverter.Classes
{
    public class ConvertedFile
    {
        public readonly FileType Type;
        public EntityAccess Access { get; private set; }
        public ClassType ClassType { get; private set; }
        public List<Property> Properties { get; private set; }


        public ConvertedFile(FileType targetType, Type type)
        {
            Type = targetType;

            LoadMetadata(type);
        }

        public void LoadMetadata(Type type)
        {
            Access = type.GetAccess();
            ClassType = type.GetClassType();

            LoadProperties(type);
        }


        private List<Property> LoadProperties(Type type)
        {
            PropertyInfo[] properties = type.GetProperties();
            List<Property> loadedProperties = new List<Property>();

            foreach(PropertyInfo property in properties)
            {
                loadedProperties.Add(LoadProperty(property));
            }

            return loadedProperties;
        }

        private Property LoadProperty(PropertyInfo property)
        {
            Property newProperty = new Property();

            newProperty.Access = property.PropertyType.GetAccess();
            newProperty.Type = property.PropertyType.GetPropertyType();

            return newProperty;
        }
    }
}
