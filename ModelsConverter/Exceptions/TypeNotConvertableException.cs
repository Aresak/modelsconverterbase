﻿using System;

namespace Symbiant.ModelsConverter.Exceptions
{
    public class TypeNotConvertableException : NotSupportedException
    {
        public TypeNotConvertableException(string message) : base(message)
        {

        }
    }
}
