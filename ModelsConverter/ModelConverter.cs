﻿using Symbiant.ModelsConverter.Classes;
using Symbiant.ModelsConverter.Enums;
using Symbiant.ModelsConverter.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Symbiant.ModelsConverter
{
    public class ModelConverter
    {
        public readonly Assembly MainAssembly = null;
        public string Destination { get; }
        public List<ConvertedFile> ConvertedFiles { get; } = new List<ConvertedFile>();
        public Type[] Types;


        public ModelConverter(Assembly assembly)
        {
            MainAssembly = assembly;
            LoadTypes(null, true);
        }

        public ModelConverter(Assembly assembly, string @namespace, bool allowNamespacesBellow)
        {
            MainAssembly = assembly;
            LoadTypes(@namespace, allowNamespacesBellow);
        }

        public ModelConverter(string @namespace, bool allowNamespacesBellow)
        {
            MainAssembly = Assembly.GetCallingAssembly();
            LoadTypes(@namespace, allowNamespacesBellow);
        }

        public ModelConverter(Type[] types)
        {
            Types = types;
        }

        public async Task<List<ConvertedFile>> Convert(FileType targetType, string targetDestination)
        {
            List<ConvertedFile> convertedFiles = new List<ConvertedFile>();

            foreach(Type type in Types)
            {
                try
                {
                    ConvertedFile file = await ConvertType(type, targetType);
                    convertedFiles.Add(file);
                }
                catch(TypeNotConvertableException)
                {

                }
            }

            return convertedFiles;
        }

        public async Task<ConvertedFile> ConvertType(Type type, FileType targetType)
        {
            ConvertedFile file = new ConvertedFile(targetType, type);
            return file;
        }

        private void LoadTypes(string @namespace, bool allowNamespacesBellow)
        {
            Types = MainAssembly.GetTypes();

            if (@namespace != null)
            {
                if (allowNamespacesBellow)
                {
                    Types = Types.Where(type => type.Namespace == @namespace).ToArray();
                }
                else
                {
                    Types = Types.Where(type => type.Namespace.StartsWith(@namespace)).ToArray();
                }
            }
        }
    }
}
