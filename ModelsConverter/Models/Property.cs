﻿using Symbiant.ModelsConverter.Enums;
using System;
using System.Collections.Generic;

namespace Symbiant.ModelsConverter.Models
{
    public class Property
    {
        /// <summary>
        /// TEST
        /// </summary>
        public EntityAccess Access { get; set; }
        public PropertyType Type { get; set; }
        public string Data { get; set; }
        public List<Comment> Comments { get; set; } = new List<Comment>();
        public string DependencyProperty { get; set; } = null;
        public Type OriginalType { get; set; }
    }
}
