﻿using Symbiant.ModelsConverter.Enums;
using System.Collections.Generic;

namespace Symbiant.ModelsConverter.Models
{
    public class Class
    {
        public EntityAccess Access { get; set; }
        public ClassType Type { get; set; }
        public string Data { get; set; }
        public List<Property> Properties { get; set; }
    }
}
