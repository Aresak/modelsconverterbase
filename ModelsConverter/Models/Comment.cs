﻿using Symbiant.ModelsConverter.Enums;

namespace Symbiant.ModelsConverter.Models
{
    public class Comment
    {
        public CommentType Type { get; set; }
        public string Data { get; set; }
    }
}
