﻿namespace Symbiant.ModelsConverter
{
    public static class Arguments
    {
        public const string Source = "-source";
        public const string TargetType = "-target";
        public const string Destination = "-destination";
    }
}
