﻿using Symbiant.ModelsConverter.Enums;
using System.IO;
using System.Reflection;
using System.Threading.Tasks;

namespace Symbiant.ModelsConverter
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Assembly mainAssembly = Assembly.GetExecutingAssembly();
            string path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

            ModelConverter converter = new ModelConverter(mainAssembly, "Symbiant.ModelsConverter.Models", true);
            await converter.Convert(FileType.TypeScript, path);
        }
    }
}
