﻿namespace Symbiant.ModelsConverter.Enums
{
    public enum ClassType
    {
        Unknown,
        Enum,
        Model
    }
}
