﻿namespace Symbiant.ModelsConverter.Enums
{
    public enum FileType
    {
        CSharp,
        TypeScript,
        Javascript
    }
}
