﻿namespace Symbiant.ModelsConverter.Enums
{
    public enum EntityAccess
    {
        Unknown,
        Public,
        Private,
        Protected
    }
}
