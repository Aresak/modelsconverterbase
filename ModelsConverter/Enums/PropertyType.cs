﻿namespace Symbiant.ModelsConverter.Enums
{
    public enum PropertyType
    {
        Byte,
        ShortByte,
        Short,
        UnsignedShort,
        Integer,
        UnsignedInteger,
        Long,
        UnsignedLong,
        Float,
        Double,
        Decimal,
        Character,
        Boolean,
        String,
        DateTime,
        Object,
        EnumNumber,
        EnumString
    }
}
