﻿namespace Symbiant.ModelsConverter.Enums
{
    public enum CommentType
    {
        Deprecated,
        Type,
        Comment
    }
}
