﻿using Symbiant.ModelsConverter.Enums;
using System;

namespace Symbiant.ModelsConverter.Extensions
{
    public static class TypeExtensions
    {
        public static EntityAccess GetAccess(this Type type)
        {
            if (type.IsPublic)
            {
                return EntityAccess.Public;
            }
            else
            {
                return EntityAccess.Unknown;
            }
        }

        public static ClassType GetClassType(this Type type)
        {
            if (type.IsEnum)
            {
                return ClassType.Enum;
            }
            else if (type.IsClass)
            {
                return ClassType.Model;
            }
            else
            {
                return ClassType.Unknown;
            }
        }

        public static PropertyType GetPropertyType(this Type type)
        {
            if(type.IsEnum)
            {
                return PropertyType.EnumNumber;
            }
            else if(type == typeof(byte))
            {
                return PropertyType.Byte;
            }
            else if(type == typeof(sbyte)) {
                return PropertyType.ShortByte;
            }
            else if(type == typeof(short))
            {
                return PropertyType.Short;
            }
            else if(type == typeof(ushort))
            {
                return PropertyType.UnsignedShort;
            }
            else if(type == typeof(int))
            {
                return PropertyType.Integer;
            }
            else if(type == typeof(uint))
            {
                return PropertyType.UnsignedInteger;
            }
            else if(type == typeof(long))
            {
                return PropertyType.Long;
            }
            else if(type == typeof(ulong))
            {
                return PropertyType.UnsignedLong;
            }
            else if(type == typeof(float))
            {
                return PropertyType.Float;
            }
            else if(type == typeof(double))
            {
                return PropertyType.Double;
            }
            else if(type == typeof(decimal))
            {
                return PropertyType.Decimal;
            }
            else if(type == typeof(char))
            {
                return PropertyType.Character;
            }
            else if(type == typeof(bool))
            {
                return PropertyType.Boolean;
            }
            else if(type == typeof(string))
            {
                return PropertyType.String;
            }
            else if(type == typeof(DateTime))
            {
                return PropertyType.DateTime;
            }
            else
            {
                return PropertyType.Object;
            }
        }
    }
}
